# Cours Interactif - Exercices Python

## 1) Première Classe

- Créer un projet sur GitLab, le cloner en local et créer un environnement virtuel dans le dossier.
- Écrire une classe `Joueur` définissant 3 attributs (nom (string), vie (int), points (int)) avec son constructeur.
- Créer des "getters" et "setters" pour chaque attribut de l'instance de classe.
- Créer une méthode pour lui ajouter des points, une pour "tuer" le joueur et une pour le "réssusciter".
- Créer une fonction pour décrire en une phrase les attributs du personnage (nom, points de vie, score).

## 2) Classe Bloc-Note

- Créer une classe `Note`.
- Créer les attributs et les méthodes nécessaires pour que la classe `Note` permette de sauvegarder des notes (str) dans un dictionnaire, consultable via une fonction demandant la clé correspondant à la valeur que l'utilisateur souhaite afficher.
- Créer un script `main.py` pour utiliser la classe `Note`.
- Intégrer les clauses TRY / EXCEPT pour les fonctions d'affichage en cas de mauvaise saisie de la clé par l'utilisateur.

## 3) Héritage

- Créer une classe mère `Life`, une classe fille `Humain` et une autre classe fille `Poisson` cohérentes entre elles.
- Pour chaque classe, écrire les getters et les setters ainsi que la méthode pour se définir (afficher les attributs de l'instance de classe).
- Créer une fonction spécifique pour chaque classe (ex: classe poisson: méthode nager()).
- Créer un script `main.py` important les trois classes et testant toutes les méthodes de chaque classe (sauf getters et setters) après instanciation des objets.

## 4) Algorithmes

### Nombre Le Plus Grand

Écrivez un programme pour trouver un maximum entre trois nombres en utilisant une if-else ou if imbriquée.

### Nombre Divisible ou Pas?

Écrivez un programme pour vérifier si un nombre est divisible par 3 et 13 ou non, en utilisant if-else.

### Nombre Pair ... ou Pas!

Écrivez un programme pour vérifier si un nombre est pair ou impair en utilisant if-else.

### Voyelle, Consonne ou Caractère Spécial

Écrivez un programme pour vérifier si un alphabet est une voyelle ou une consonne en utilisant if-else. Les lettres a, e, i, o et u en minuscules et en majuscules sont appelées voyelles. Les alphabets autres que les voyelles sont appelés consonnes.

### Jour de la Semaine

Écrivez un programme pour entrer le numéro du jour de la semaine (1-7) et affichez le nom du jour de la semaine correspondant en utilisant if-else puis switch-case.

### Nombre de Jours dans le Mois

Écrivez un programme pour entrer le numéro du mois entre (1-12) et afficher le nombre de jours de ce mois en utilisant if-else.

### Profit ou Perte?

Écrivez un programme pour saisir le prix de fabrication et le prix de vente d'un produit et vérifiez le profit ou la perte. Si le prix de fabrication est supérieur au prix de vente, il y a perte sinon profit.

### Chiffre, Lettre ou Autre?

Écrivez un programme pour saisir un caractère de l'utilisateur et vérifiez si le caractère donné est un alphabet, un chiffre ou un caractère spécial en utilisant if-else, puis switch-case.
  - Un caractère est un alphabet s'il se situe entre a-z ou A-Z.
  - Un caractère est un chiffre s'il est compris entre 0 et 9.
  - Un caractère est un symbole spécial s'il est ni alphabet ni chiffre.

## Aller Plus Loin

Vous trouverez dans les liens ci-dessous des sites proposant des "challenges" pour pousser vos connaissances dans des langages particuliers au choix.

- Note: (nécéssite de s'inscrire mais le service reste gratuit)
  - [Codingame](https://www.codingame.com/)
  - [Codewars](https://www.codewars.com/)

